<?php
class Cadena{
    private $valor;
    private $longitud;
    private $vocales;
    
    function __construct($valor) {
        $this->setValor($valor);
    }
    public function getValor($minusculas=FALSE){
        if($minusculas){
            return strtolower($this->valor);
        }else{
            return $this->valor;
        }
    }
    public function getLongitud(){
        $this->calcularLongitud();
        return $this->longitud;
        }
    public function getVocales(){
        $this->numeroVocales();
        return $this->vocales;
    }
    public  function setValor($valor){
        $this->valor=$valor;
    }
    public function setLongitud($longitud){
        $this->longitud=$longitud;
        }

    public function setVocales($vocales){
        $this->vocales=$vocales;
    }
    private function calcularLongitud(){
        $rhis->setLongitud(strlen($this->valor));
    }
    private function numeroVocales(){
        $vocales=Array('a','e','i','o','u');
        $longitud=0;
        foreach ($vocales as $valor){
            $longitud+= substr_count($this->getValor(1), $valor);
        }
        $this->setVocales($longitud);
    }
    public function repeticionVocal($vocal){
        $longitud=0;
        $longitud+= substr_count($this->getValor(1), $vocal);
        return $longitud;
    }
    
}